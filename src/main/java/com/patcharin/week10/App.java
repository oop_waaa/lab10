package com.patcharin.week10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec);
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2);
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());

        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%s area %.3f \n", circle1.getName(), circle1.calArea()); //printf %.3f คือการทำจุดทศนิยม 
        System.out.printf("%s perimeter %.3f \n", circle1.getName(),circle1.calPerimeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%s area %.3f \n", circle1.getName(),circle2.calArea());
        System.out.printf("%s perimeter %.3f \n", circle1.getName(),circle2.calPerimeter());

        Triangle tri = new Triangle(2,2,2);
        System.out.println(tri);
        System.out.printf("%s area %.3f \n",tri.getName(),tri.calArea());
        System.out.printf("%s area %.3f \n",tri.getName(),tri.calPerimeter());
    }
}
